document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.collapsible');
    var instance = M.Collapsible.init(elems, {
        accordion: false
    });
});


$(document).ready(function () {

    app.more()
});
var app = new Vue({
    el: "#app",
    data: {
        movies: [
        ],
        page: 0
    },
    methods: {
        more: function () {
            this.page++
            $.ajax({
                type: "POST",
                url: 'https://floating-badlands-53340.herokuapp.com/sendmovies',
                data: {
                    page: this.page, genres: filters.genreflt, countries: filters.countryflt, min: filters.minRange,
                    max: filters.maxRange, search: search.search
                },
                success: (res) => {
                    for (i = 0; i < res.length; i++) {
                        this.movies.push(res[i]);
                    }
                },
                error: (er) => {
                    console.log(er.status)
                }
            })

        }
    }
})

var search = new Vue({
    el: '#search',
    data: { search: '' },
    watch: {
        search: function () {
            app.page = 1
            $.ajax({
                type: "POST",
                url: 'https://floating-badlands-53340.herokuapp.com/sendmovies',
                data: {
                    page: app.page, genres: filters.genreflt, countries: filters.countryflt, min: filters.minRange,
                    max: filters.maxRange, search: this.search
                },
                success: (res) => {
                    app.movies = []
                    for (i = 0; i < res.length; i++) {
                        app.movies.push(res[i]);
                    }
                },
                error: (er) => {
                    console.log(er.status)
                }
            })

        }
    }
})

var filters = new Vue({
    el: '#filters',
    data: {
        genrelist: ['Animation', 'Comedy', 'Family', 'Action', 'Crime', 'Drama', 'Thriller', 'Romance', 'Adventure', 'War',
            'Fantasy', 'Music', 'History', 'Horror', ' Documentary', 'Science Fiction', 'Mystery', 'Foreign', 'Western', 'TV Movie'],
        countrylist: ['United States of America', 'France', 'Italy', 'United Kingdom', 'Germany', 'China', 'Netherlands',
            'Belgium', 'South Africa', 'Canada', 'Japan', 'Hong Kong', 'Spain', 'Australia', 'Ireland', 'Croatia', 'Iran', 'Russia',
            'Switzerland', 'Tunisia', 'Dominican Republic', 'Taiwan', 'Poland', 'New Zealand', 'Austria', 'Liechtenstein', 'Macedonia',
            'Cuba', 'Mexico', 'Peru', 'Denmark', 'Sweden', 'Portugal', 'South Korea', 'Argentina', 'India', 'Finland', 'Burkina Faso', 'Brazil',
            'Romania', 'Iceland', 'Serbia', 'Hungary', 'Czech Republic', 'Senegal', 'Congo', 'Vietnam', 'Trinidad and Tobago', 'Greece', 'Philippines',
            'Chile', 'Norway', 'Bulgaria', 'Kazakhstan', 'Georgia', 'Ukraine', 'Algeria', 'Luxembourg', 'Botswana', 'Aruba', 'Israel', 'Turkey',
            'Ecuador', 'Morocco', 'Bahamas', 'Bosnia and Herzegovina', 'Lebanon', 'Malaysia', 'Bhutan', 'Jamaica', 'Pakistan', 'Nepal',
            'Cote DIvoire', 'Thailand', 'Namibia', 'Cameroon', 'Uruguay', 'Colombia', 'Czechoslovakia', 'Slovenia', 'Libyan Arab Jamahiriya',
            'Puerto Rico', 'Soviet Union', 'East Germany', 'Singapore', 'Afghanistan', 'Panama', 'Malta', 'Egypt', 'Zimbabwe', 'Tajikistan',
            'Uzbekistan', 'Costa Rica', 'Ghana', 'Kuwait', 'Martinique', 'Indonesia', 'Armenia', 'Mongolia', 'Slovakia', 'Monaco', 'Lithuania',
            'Serbia and Montenegro', 'Venezuela', 'Rwanda', 'Palestinian Territory', 'Chad', 'Paraguay', 'Qatar', 'Estonia', 'Bolivia', 'Iraq',
            'Macao', 'Mali', 'Nicaragua', 'United States Minor Outlying Islands', 'United Arab Emirates', 'Liberia', 'Yugoslavia', 'Angola',
            'Azerbaijan', 'Belarus', 'Cambodia', 'Cayman Islands', 'Montenegro', 'Mauritania', 'Bangladesh', 'Albania', 'Tanzania', 'Cyprus',
            'Syrian Arab Republic', 'North Korea', 'Kenya', 'Jordan', 'Latvia', 'Guatemala', 'Papua New Guinea', 'Nigeria', 'Uganda', 'Ethiopia',
            'Lao Peoples Democratic Republic', 'French Polynesia', 'Netherlands Antilles', 'Myanmar', 'Kyrgyz Republic', 'El Salvador', 'Moldova',
            'Samoa', 'Antarctica', 'Gibraltar', 'Brunei Darussalam', 'Saudi Arabia', 'Honduras', 'Sri Lanka', 'Somalia', 'Madagascar', 'Bermuda',
            'French Southern Territories', 'Barbados', 'Guinea'
        ],
        genreflt: ['Animation', 'Comedy', 'Family', 'Action', 'Crime', 'Drama', 'Thriller', 'Romance', 'Adventure', 'War',
            'Fantasy', 'Music', 'History', 'Horror', ' Documentary', 'Science Fiction', 'Mystery', 'Foreign', 'Western', 'TV Movie'],
        countryflt: ['United States of America', 'France', 'Italy', 'United Kingdom', 'Germany', 'China', 'Netherlands',
            'Belgium', 'South Africa', 'Canada', 'Japan', 'Hong Kong', 'Spain', 'Australia', 'Ireland', 'Croatia', 'Iran', 'Russia',
            'Switzerland', 'Tunisia', 'Dominican Republic', 'Taiwan', 'Poland', 'New Zealand', 'Austria', 'Liechtenstein', 'Macedonia',
            'Cuba', 'Mexico', 'Peru', 'Denmark', 'Sweden', 'Portugal', 'South Korea', 'Argentina', 'India', 'Finland', 'Burkina Faso', 'Brazil',
            'Romania', 'Iceland', 'Serbia', 'Hungary', 'Czech Republic', 'Senegal', 'Congo', 'Vietnam', 'Trinidad and Tobago', 'Greece', 'Philippines',
            'Chile', 'Norway', 'Bulgaria', 'Kazakhstan', 'Georgia', 'Ukraine', 'Algeria', 'Luxembourg', 'Botswana', 'Aruba', 'Israel', 'Turkey',
            'Ecuador', 'Morocco', 'Bahamas', 'Bosnia and Herzegovina', 'Lebanon', 'Malaysia', 'Bhutan', 'Jamaica', 'Pakistan', 'Nepal',
            'Cote DIvoire', 'Thailand', 'Namibia', 'Cameroon', 'Uruguay', 'Colombia', 'Czechoslovakia', 'Slovenia', 'Libyan Arab Jamahiriya',
            'Puerto Rico', 'Soviet Union', 'East Germany', 'Singapore', 'Afghanistan', 'Panama', 'Malta', 'Egypt', 'Zimbabwe', 'Tajikistan',
            'Uzbekistan', 'Costa Rica', 'Ghana', 'Kuwait', 'Martinique', 'Indonesia', 'Armenia', 'Mongolia', 'Slovakia', 'Monaco', 'Lithuania',
            'Serbia and Montenegro', 'Venezuela', 'Rwanda', 'Palestinian Territory', 'Chad', 'Paraguay', 'Qatar', 'Estonia', 'Bolivia', 'Iraq',
            'Macao', 'Mali', 'Nicaragua', 'United States Minor Outlying Islands', 'United Arab Emirates', 'Liberia', 'Yugoslavia', 'Angola',
            'Azerbaijan', 'Belarus', 'Cambodia', 'Cayman Islands', 'Montenegro', 'Mauritania', 'Bangladesh', 'Albania', 'Tanzania', 'Cyprus',
            'Syrian Arab Republic', 'North Korea', 'Kenya', 'Jordan', 'Latvia', 'Guatemala', 'Papua New Guinea', 'Nigeria', 'Uganda', 'Ethiopia',
            'Lao Peoples Democratic Republic', 'French Polynesia', 'Netherlands Antilles', 'Myanmar', 'Kyrgyz Republic', 'El Salvador', 'Moldova',
            'Samoa', 'Antarctica', 'Gibraltar', 'Brunei Darussalam', 'Saudi Arabia', 'Honduras', 'Sri Lanka', 'Somalia', 'Madagascar', 'Bermuda',
            'French Southern Territories', 'Barbados', 'Guinea'
        ],
        minRange: 1874,
        maxRange: 2020,
        slider: {
            startMin: 1874,
            startMax: 2020,
            min: 1874,
            max: 2020,
            step: 1
        },
        icong: "clear",
        iconc: "clear",
        messageg: "Select None",
        messagec: "Select None"

    },
    mounted: function () {
        noUiSlider.create(this.$refs.slider, {
            start: [this.slider.startMin, this.slider.startMax],
            step: this.slider.step,
            range: {
                'min': this.slider.min,
                'max': this.slider.max
            },
            connect: true,
            orientation: 'horizontal',
            format: wNumb({
                decimals: 0
            })
        })
        this.$refs.slider.noUiSlider.on('change', (values, handle) => {
            this[handle ? 'maxRange' : 'minRange'] = parseInt(values[handle]);

        })
        this.$refs.slider.noUiSlider.on('change', () => {

            app.page = 1
            $.ajax({
                type: "POST",
                url: 'https://floating-badlands-53340.herokuapp.com/sendmovies',
                data: {
                    page: app.page, genres: this.genreflt, countries: this.countryflt, min: this.minRange,
                    max: this.maxRange, search: search.search
                },
                success: (res) => {
                    app.movies = []
                    for (i = 0; i < res.length; i++) {
                        app.movies.push(res[i]);
                    }
                },
                error: (er) => {
                    console.log(er.status)
                }
            })


        })

    },
    methods: {
        updatefl: function () {
            if (this.genreflt.length != 0) {
                this.icong = "clear"
                this.messageg = "Select None"
            }
            if (this.countryflt.length != 0) {
                this.iconc = "clear"
                this.messagec = "Selevt None"
            }
            app.page = 1
            $.ajax({
                type: "POST",
                url: 'https://floating-badlands-53340.herokuapp.com/sendmovies',
                data: {
                    page: app.page, genres: this.genreflt, countries: this.countryflt, min: this.minRange,
                    max: this.maxRange, search: search.search
                },
                success: (res) => {
                    app.movies = []
                    for (i = 0; i < res.length; i++) {
                        app.movies.push(res[i]);
                    }
                },
                error: (er) => {
                    console.log(er.status)
                }
            })

        },
        clear_countries: function () {
            if (this.countryflt.length == 0) {
                this.countryflt = this.countrylist
                this.iconc = "clear"
                this.messagec = "Select None"
            } else {
                this.countryflt = []
                this.iconc = "check"
                this.messagec = "Select All"
            }
            this.updatefl()

        },
        clear_genres: function () {
            if (this.genreflt.length == 0) {
                this.genreflt = this.genrelist
                this.icong = "clear"
                this.messageg = "Select None"

            } else {
                this.genreflt = []
                this.icong = "check"
                this.messageg = "Select All"

            }
            this.updatefl()

        }
    }
})