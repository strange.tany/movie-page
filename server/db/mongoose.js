const mongoose = require('mongoose')

mongoose.set('debug', true)

options = {
  server: {
      socketOptions: {
          keepAlive: 1
      }
  }
}

mongoose.connect('mongodb://strange:movies1@ds125693.mlab.com:25693/movies_metadata',{useNewUrlParser: true}), options

module.exports = mongoose