const mongoose = require('./mongoose')

const films = new mongoose.Schema({
    genres: [{
        'name': {
            type: String,
            default: ""
        }

    }],
    poster_path: {
        type: String
    },
    production_countries: [{
        'name': {
            type: String,
            default: ""
        }

    }],
    release_date: {
        type: Number
    },
    title: {
        type: String
    },
    vote_average: {
        type: Number
    }
})

exports.films = mongoose.model('films', films)
