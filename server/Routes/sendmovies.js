const films = require('../db/schema').films
exports.post = (req, res, next) => {
    filters = req.body
    if(filters.countries==undefined){
        filters.countries = []
    }
    page = filters.page
    films.find({
        'release_date': { "$gte": filters.min, "$lte": filters.max },
        'production_countries.name': {'$in': filters.countries},
        'genres.name':{'$in': filters.genres},
        'title': {'$regex': filters.search, '$options': 'i'}
    })
        .sort({ 'vote_average': -1 }).limit(20 * page).exec((err, movie) => {
            sendMovie = movie.slice(20 * page - 20, 20 * page)
            res.send(sendMovie).status(200)

        })
}