const Router = require('express').Router()
const path = require('path')

const sendmovies = require('./sendmovies')
Router.get('/', (req, res, next)=>{
    res.sendFile(path.join(__dirname, '..', '..', 'main_page.html'))
})

Router.post('/sendmovies', sendmovies.post)

module.exports = Router