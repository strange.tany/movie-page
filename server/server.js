const express = require('express')
const app = express()
const Server = require('http').Server(app)
const parser = require('body-parser')
const path = require('path')

const page = require('./Routes/page')

const options = require('./options')
app.use(parser.urlencoded(options.parser))
app.use(parser.json())

app.use('/static', express.static(path.join(__dirname, '..')))


app.use('/', page)
const port = process.env.PORT || 8000
Server.listen(port, () => {
  console.log('127.0.0.1:3000')
})